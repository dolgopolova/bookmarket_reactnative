/**
 * Metro configuration for React Native
 * https://github.com/facebook/react-native
 *
 * @format
 */
// const defaultSourceExts = require("metro-config/src/defaults/defaults").sourceExts;
// const defaultAssetExts = require("metro-config/src/defaults/defaults").assetExts;

// resolver: {
//   sourceExts: [...defaultSourceExts],
//   assetExts: [
//       ...defaultAssetExts,
//       'md'
//   ]
// }
module.exports = {
  transformer: {
    getTransformOptions: async () => ({
      transform: {
        experimentalImportSupport: false,
        inlineRequires: false,
      },
    }),
  }
};