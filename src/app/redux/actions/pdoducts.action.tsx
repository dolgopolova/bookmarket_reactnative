import { Alert } from "react-native";
import ProductsService from "../../services/products.service"
import ProductModel from "src/app/shared/models/products/product.model";

export function getAllProducts() {
    return async (dispatch: any) => {
        await ProductsService.getAllProducts()
        .then((response: any) => dispatch(getAllProductsComplete(response)))
        .catch((error: any) => Alert.alert(error.response.data));
    }
}
export function getAllProductsComplete(response: ProductModel) {
    return { type: 'GET_ALL_PRODUCTS_COMPLETE', products: response};
}