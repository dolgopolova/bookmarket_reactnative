import SignUpModel from "../../shared/models/auth/signUp.model";
import AuthService from "../../services/auth.service";
import TokenAuthModel from "../../shared/models/auth/tokenAuth.model";
import SignInModel from "../../shared/models/auth/signIn.model";
import { Alert } from "react-native";

export function signIn(data: SignInModel) {
    return async (dispatch: any) => {
        await AuthService.signIn(data)
        .then(response => dispatch(signInSuccess(response)))
        .catch(error => Alert.alert(error.response));
    }
}
export function signInSuccess(response: TokenAuthModel) {
    return { type: 'SIGNIN_SUCCESS', signInData: response};
}

export function signUp(model: SignUpModel) {
    return async (dispatch: any) => {
        await AuthService.signUp(model)
        .then(() => dispatch(signUpComplete(true)))
        .catch(() => dispatch(signUpComplete(false)));
    }
}
export function signUpComplete(response: boolean) {
    return { type: 'SIGNUP', signUpData: response};
}