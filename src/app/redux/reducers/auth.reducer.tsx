import AuthDefaultState from "../states/auth.state";

const AuthReducer = (state = AuthDefaultState, action: any) => {
    switch (action.type) {
        case "SIGNIN_SUCCESS": {
            return {
                ...state,
                isLoggedIn: true,
                userDataSignIn: action.signInData
            }
        }
        case "SIGNUP": {
            return {
                ...state,
                isLoggedIn: false,
                userDataSignUp: action.signUpData
            }
        }
        default: return { ...state };
    }
}

export default AuthReducer;