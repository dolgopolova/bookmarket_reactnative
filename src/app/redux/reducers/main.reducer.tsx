import AuthReducer from "./auth.reducer";
import ProductsReducer from "./products.reducer";
import DefaultState from "../states/default.state";

const MainReducer = (state = DefaultState, action) => {
    switch(action.type) {
        case "SIGNIN_SUCCESS": {
            return {
                ...state,
                auth: AuthReducer(state.auth, action)
            }
        }
        case "SIGNUP": {
            return {
                ...state,
                auth: AuthReducer(state.auth, action)
            }
        }
        case "GET_ALL_PRODUCTS_COMPLETE": {
            return {
                ...state,
                products: ProductsReducer(state.products, action)
            }
        }
        default: return { ...state }
    }
}

export default MainReducer;