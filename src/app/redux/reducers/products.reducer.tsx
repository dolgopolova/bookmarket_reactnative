import ProductsDefaultState from "../states/products.state";

const ProductsReducer = (state = ProductsDefaultState, action: any) => {
    switch (action.type) {
        case "GET_ALL_PRODUCTS_COMPLETE": {
            return {
                ...state,
                books: action.products.books,
                magazines: action.products.magazines
            }
        }
        default: return { ...state };
    }
}

export default ProductsReducer;