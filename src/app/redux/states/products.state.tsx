import BookModel from "src/app/shared/models/products/book.model";
import MagazineModel from "src/app/shared/models/products/magazine.model";

let ProductsDefaultState = {
    books: Array<BookModel>(),
    magazines: Array<MagazineModel>()
}

export default ProductsDefaultState;