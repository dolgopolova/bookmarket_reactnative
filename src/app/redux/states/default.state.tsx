import AuthDefaultState from "./auth.state";
import ProductsDefaultState from "./products.state";

let DefaultState = {
    auth: AuthDefaultState,
    products: ProductsDefaultState
}

export default DefaultState;