import SignInModel from "../../shared/models/auth/signIn.model";
import SignUpModel from "../../shared/models/auth/signUp.model";

let AuthDefaultState = {
    isLoggedIn: false,
    userDataSignIn: SignInModel,
    userDataSignUp: SignUpModel
}

export default AuthDefaultState;