
enum Tabs {
    Cart = 'Cart',
    Home = 'Home',
    Profile = 'Profile'
}

export default Tabs;