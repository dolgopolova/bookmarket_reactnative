import React, { Component } from "react";
import { View } from "react-native";
import { connect } from "react-redux";
import { NavigationScreenProp, NavigationState, NavigationParams } from "react-navigation";
import BottomNavigation, { FullTab } from 'react-native-material-bottom-navigation';
import Icon from "react-native-vector-icons/FontAwesome";
import Tabs from "../../core/enums/tabs.enum";
import ProductsView from "./Products/products.view"
import CartView from "./Cart/cart.view"
import ProfileView from "./Profile/profile.view"

interface Props {
  navigation: NavigationScreenProp<NavigationState, NavigationParams>,
}

interface State {
  activeTab: Tabs
}

class HomeScreen extends Component<Props, State> {
  static navigationOptions = {
    header: null,
  };

  constructor(props: Props) {
    super(props);
    this.state = {
      activeTab: Tabs.Home
    }
  }

  tabs = [
    {
      key: 'Cart',
      icon: 'shopping-cart',
      label: 'Cart',
      barColor: '#95BDBC',
      pressColor: 'rgba(255, 255, 255, 0.16)'
    },
    {
      key: 'Home',
      icon: 'home',
      label: 'Home',
      barColor: '#A1D2CF',
      pressColor: 'rgba(255, 255, 255, 0.16)'
    },
    {
      key: 'Profile',
      icon: 'user',
      label: 'Profile',
      barColor: '#B7898B',
      pressColor: 'rgba(255, 255, 255, 0.16)'
    }
  ]

  renderIcon = (icon: any) => () => (
    <Icon size={24} color="white" name={icon} />
  )

  renderTab = ({ tab, isActive }) => (
    <FullTab
      isActive={isActive}
      key={tab.key}
      label={tab.label}
      renderIcon={this.renderIcon(tab.icon)}
    />
  )

  render() {
    return (
      <View style={{ flex: 1 }}>
        <View style={{ flex: 1 }}>
          { this.state.activeTab === Tabs.Cart && <CartView></CartView>}
          { this.state.activeTab === Tabs.Home && <ProductsView></ProductsView>}
          { this.state.activeTab === Tabs.Profile && <ProfileView></ProfileView>}
        </View>
        <BottomNavigation
          onTabPress={newTab => this.setState({ activeTab: Tabs[newTab.key] })}
          activeTab={ this.state.activeTab.valueOf() }
          renderTab={this.renderTab}
          tabs={this.tabs}
        />
      </View>
    );
  }
}

const mapStateToProps = (state: any) => ({

});

const mapDispatchToProps = (dispatch: any) => ({

});

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen);