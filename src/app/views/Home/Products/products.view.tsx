import { Component } from "react";
import { connect } from "react-redux";
import React from "react";
import { View, ScrollView } from "react-native";
import * as actions from "../../../redux/actions/pdoducts.action"
import BookModel from "../../../shared/models/products/book.model";
import MagazineModel from "../../../shared/models/products/magazine.model";
import ProductComponent from "../../../components/product/product.component";

interface Props {
    getAllProducts: () => void,
    bookList: Array<BookModel>,
    magazineList: Array<MagazineModel>
}

interface State {
}

class ProductsView extends Component<Props, State> {
    constructor(props: Props) {
        super(props);
    }

    public componentDidMount() {
        this.props.getAllProducts();
    }

    public componentDidUpdate(prevProps: any) { 
    }

    public render() {
        return (
            <ScrollView style={{ flex: 1 }}>
                <View style={{ flex: 1 }}>
                    {
                        this.props.bookList.map((book, index) => {
                            return (
                                <ProductComponent 
                                    key={index} type="Book"
                                    name={book.name} author={book.authors} 
                                    price={book.price} category={book.category}>
                                </ProductComponent>
                            );
                        })
                    }
                    {
                        this.props.magazineList.map((magazine, index) => {
                            return (
                                <ProductComponent 
                                    key={index} type="Magazine"
                                    name={magazine.name} author={magazine.publisher} 
                                    price={magazine.price} category={magazine.category}>
                                </ProductComponent>
                            );
                        })
                    }
                </View>
            </ScrollView>
        );
    }

}

const mapStateToProps = (state: any) => ({
    bookList: state.products.books,
    magazineList: state.products.magazines
});

const mapDispatchToProps = (dispatch: any) => ({
    getAllProducts: () => dispatch(actions.getAllProducts())
});

export default connect(mapStateToProps, mapDispatchToProps)(ProductsView);