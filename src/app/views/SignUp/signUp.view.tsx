import React, { Component } from "react";
import { TextInput, View, TouchableWithoutFeedback, Keyboard, Text, TouchableOpacity } from "react-native";
import styles from "./style";
import * as actions from "../../redux/actions/auth.action"
import SignUpModel from "../../shared/models/auth/signUp.model";
import { NavigationScreenProp, NavigationState, NavigationParams, ScrollView } from "react-navigation";
import { connect } from "react-redux";
import ShowError from "../../shared/models/auth/showError.model";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

interface Props {
  navigation: NavigationScreenProp<NavigationState, NavigationParams>,
  signUp: (data: SignUpModel) => void
}

interface State {
  firstName: string,
  lastName: string,
  email: string,
  password: string,
  confirmPassword: string,

  firstNameError: ShowError,
  lastNameError: ShowError,
  emailError: ShowError,
  passwordError: ShowError,
  confirmPasswordError: ShowError
}

class SignUpScreen extends Component<Props, State> {
  static navigationOptions = ({ navigation }) => {
    return {
      title: 'Registration',
      headerTintColor: '#ffffff',
      headerStyle: {
        backgroundColor: '#7787A0',
      },
      headerRight: (
        <TouchableOpacity
          onPress={navigation.getParam('onSignUpPress')}
          style={styles.signUpButton}>
            <Text style={styles.signUpText}>Register</Text>
        </TouchableOpacity>
      ),
    }
  };
  private secondTextInput!: TextInput;
  private thirdTextInput!: TextInput;
  private fourTextInput!: TextInput;
  private fiveTextInput!: TextInput;

  constructor(props: Props) {
    super(props);
    this.props.navigation.setParams({ onSignUpPress: this.onSignUpPress });
    this.state = {
      firstName: '',
      lastName: '',
      email: '',
      password: '',
      confirmPassword: '',
      firstNameError: new ShowError(),
      lastNameError: new ShowError(),
      emailError: new ShowError(),
      passwordError: new ShowError(),
      confirmPasswordError: new ShowError()
    }
  }

  render() {
    return (
      <KeyboardAwareScrollView
        resetScrollToCoords={{ x: 0, y: 0 }}
        contentContainerStyle={styles.container}
        scrollEnabled={true} >
        <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
          <ScrollView contentContainerStyle={{ flexGrow: 1, justifyContent: 'center', alignItems: 'center' }}>
            <View>
              <TextInput
                placeholder="Email"
                placeholderTextColor="#c4c3cb"
                keyboardType="email-address"
                returnKeyType="next"
                style={styles.input}
                onSubmitEditing={() => { this.secondTextInput.focus(); }}
                onChangeText={(text) => {
                  this.setState(prevState => {
                    let emailError = Object.assign({}, prevState.emailError);
                    emailError.message = '';
                    emailError.shouldShow = false;
                    return { emailError };
                  });
                  this.setState({ email: text });
                }}
              />
              {this.state.emailError.shouldShow &&
                <Text style={styles.errorText}>{this.state.emailError.message}</Text>
              }
              <TextInput
                placeholder="First Name"
                placeholderTextColor="#c4c3cb"
                keyboardType="default"
                returnKeyType="next"
                style={styles.input}
                ref={(input) => { this.secondTextInput = input; }}
                onSubmitEditing={() => { this.thirdTextInput.focus(); }}
                onChangeText={(text) => {
                  this.setState(prevState => {
                    let firstNameError = Object.assign({}, prevState.firstNameError);
                    firstNameError.message = '';
                    firstNameError.shouldShow = false;
                    return { firstNameError };
                  });
                  this.setState({ firstName: text });
                }}
              />
              {this.state.firstNameError.shouldShow &&
                <Text style={styles.errorText}>{this.state.firstNameError.message}</Text>
              }
              <TextInput
                placeholder="Last Name"
                placeholderTextColor="#c4c3cb"
                keyboardType="default"
                returnKeyType="next"
                ref={(input) => { this.thirdTextInput = input; }}
                onSubmitEditing={() => { this.fourTextInput.focus(); }}
                style={styles.input}
                onChangeText={(text) => {
                  this.setState(prevState => {
                    let lastNameError = Object.assign({}, prevState.lastNameError);
                    lastNameError.message = '';
                    lastNameError.shouldShow = false;
                    return { lastNameError };
                  });
                  this.setState({ lastName: text })
                }}
              />
              {this.state.lastNameError.shouldShow &&
                <Text style={styles.errorText}>{this.state.lastNameError.message}</Text>
              }
              <TextInput
                placeholder="Password"
                placeholderTextColor="#c4c3cb"
                keyboardType="default"
                returnKeyType="next"
                ref={(input) => { this.fourTextInput = input; }}
                onSubmitEditing={() => { this.fiveTextInput.focus(); }}
                style={styles.input}
                onChangeText={(text) => {
                  this.setState(prevState => {
                    let passwordError = Object.assign({}, prevState.passwordError);
                    passwordError.message = '';
                    passwordError.shouldShow = false;
                    return { passwordError };
                  });
                  this.setState({ password: text });
                }}
              />
              {this.state.passwordError.shouldShow &&
                <Text style={styles.errorText}>{this.state.passwordError.message}</Text>
              }
              <TextInput
                placeholder="Confirm Password"
                placeholderTextColor="#c4c3cb"
                keyboardType="default"
                returnKeyType="done"
                ref={(input) => { this.fiveTextInput = input; }}
                onSubmitEditing={() => { this.onSignUpPress(); }}
                style={styles.input}
                onChangeText={(text) => {
                  this.setState(prevState => {
                    let confirmPasswordError = Object.assign({}, prevState.confirmPasswordError);
                    confirmPasswordError.message = '';
                    confirmPasswordError.shouldShow = false;
                    return { confirmPasswordError };
                  });
                  this.setState({ confirmPassword: text });
                }}
              />
              {this.state.confirmPasswordError.shouldShow &&
                <Text style={styles.errorText}>{this.state.confirmPasswordError.message}</Text>
              }
            </View>
          </ScrollView>
        </TouchableWithoutFeedback>
      </KeyboardAwareScrollView>
    );
  }

  private onSignUpPress = async () => {
    let isEmailValid = this.validateEmail();
    let isPasswordValid = this.validatePassword();
    let isConfirmPasswordValid = this.validateConfirmPassword();
    let isFirstNameValid = this.validateFirstName();
    let isLastNameValid = this.validateLastName();

    if (!(isEmailValid && isPasswordValid && isConfirmPasswordValid && isFirstNameValid && isLastNameValid)) {
      return;
    }

    await this.props.signUp(this.state);
    this.props.navigation.navigate('SignIn');
  }

  private validateEmail(): boolean {
    let pattern = /^[a-zA-Z0-9]+([-._]?[a-zA-Z0-9]+)*@[a-z0-9]+[-]?([-]?[a-z0-9])+[.]([a-z]{2,})$/;
    if (this.state.email.replace(' ', '') === '') {
      this.setState(prevState => {
        let emailError = Object.assign({}, prevState.emailError);
        emailError.message = 'The field cannot be empty';
        emailError.shouldShow = true;
        return { emailError };
      });
      return false;
    }
    if (!pattern.test(this.state.email)) {
      this.setState(prevState => {
        let emailError = Object.assign({}, prevState.emailError);
        emailError.message = 'The field must contain @ and . symbols';
        emailError.shouldShow = true;
        return { emailError };
      });
      return false;
    }

    return true;
  }

  private validatePassword(): boolean {
    if (this.state.password.replace(' ', '') === '') {
      this.setState(prevState => {
        let passwordError = Object.assign({}, prevState.passwordError);
        passwordError.message = 'The field cannot be empty';
        passwordError.shouldShow = true;
        return { passwordError };
      });
      return false;
    }
    if (this.state.password.length < 6) {
      this.setState(prevState => {
        let passwordError = Object.assign({}, prevState.passwordError);
        passwordError.message = 'Password must be more than 6 symbols';
        passwordError.shouldShow = true;
        return { passwordError };
      });
      return false
    }

    return true;
  }

  private validateConfirmPassword(): boolean {
    if (this.state.confirmPassword.replace(' ', '') === '') {
      this.setState(prevState => {
        let confirmPasswordError = Object.assign({}, prevState.confirmPasswordError);
        confirmPasswordError.message = 'The field cannot be empty';
        confirmPasswordError.shouldShow = true;
        return { confirmPasswordError };
      });
      return false;
    }
    if (this.state.password != this.state.confirmPassword) {
      this.setState(prevState => {
        let confirmPasswordError = Object.assign({}, prevState.confirmPasswordError);
        confirmPasswordError.message = 'Confirm password and password are not equal';
        confirmPasswordError.shouldShow = true;
        return { confirmPasswordError };
      });
      return false;
    }

    return true;
  }

  private validateFirstName(): boolean {
    if (this.state.firstName.replace(' ', '') === '') {
      this.setState(prevState => {
        let firstNameError = Object.assign({}, prevState.firstNameError);
        firstNameError.message = 'The field cannot be empty';
        firstNameError.shouldShow = true;
        return { firstNameError };
      });
      return false;
    }
    return true;

  }

  private validateLastName(): boolean {
    if (this.state.lastName.replace(' ', '') === '') {
      this.setState(prevState => {
        let lastNameError = Object.assign({}, prevState.lastNameError);
        lastNameError.message = 'The field cannot be empty';
        lastNameError.shouldShow = true;
        return { lastNameError };
      });
      return false;
    }
    return true;
  }
}

const mapStateToProps = (state: any) => ({
});

const mapDispatchToProps = (dispatch: any) => ({
  signUp: (data: SignUpModel) => dispatch(actions.signUp(data))
});

export default connect(mapStateToProps, mapDispatchToProps)(SignUpScreen);