import { Dimensions } from "react-native";

const React = require("react-native");
const { StyleSheet } = React;
const window = Dimensions.get('window');

export default StyleSheet.create({
    container: {
        backgroundColor: '#c3d4d9',
        flex: 1
    },
    input: {
        width: window.width - 30,
        height: 43,
        fontSize: 14,
        borderRadius: 5,
        borderWidth: 1,
        borderColor: '#eaeaea',
        backgroundColor: '#fafafa',
        marginTop: 10,
        marginHorizontal: 10,
        marginVertical: 5,
    },
    signUpButton: {
        backgroundColor: 'transparent',
        margin: 10
    },
    signUpText: {
        color: 'white'
    },
    errorText: {
        width: window.width - 30,
        color: 'red',
    }
});