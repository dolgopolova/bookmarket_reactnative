import React, { Component } from "react";
import styles from "./style";
import { Keyboard, Text, View, TextInput, TouchableWithoutFeedback, Alert, TouchableOpacity } from 'react-native';
import { connect } from "react-redux";
import * as actions from "../../redux/actions/auth.action"
import SignInModel from "../../shared/models/auth/signIn.model";
import { NavigationScreenProp, NavigationState, NavigationParams, ScrollView } from "react-navigation";

interface Props {
  navigation: NavigationScreenProp<NavigationState, NavigationParams>,
  isLoggedIn: boolean,
  signIn: (data: SignInModel) => void
}

class SignInScreen extends Component<Props, SignInModel> {
  static navigationOptions = {
    header: null,
  };
  private secondTextInput: any;

  constructor(props: Props) {
    super(props);
    this.state = new SignInModel();
  }

  render() {
    return (
      <ScrollView style={{ flex: 1 }}>
        <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
          <View style={styles.loginScreenContainer}>
            <View style={styles.loginFormView}>
              <Text style={styles.logoText}>Book Market</Text>
              <TextInput
                placeholder="Email" placeholderTextColor="#c4c3cb"
                returnKeyType={"next"}
                onSubmitEditing={() => { this.secondTextInput.focus(); }}
                blurOnSubmit={false}
                style={styles.loginFormTextInput}
                onChangeText={(text) => this.setState({ email: text })}
              />
              <TextInput
                placeholder="Password" placeholderTextColor="#c4c3cb"
                ref={(input) => { this.secondTextInput = input; }}
                blurOnSubmit={true}
                returnKeyType={"done"}
                onSubmitEditing={() => { this.onSignInPress() }}
                style={styles.loginFormTextInput} secureTextEntry={true}
                onChangeText={(text) => this.setState({ password: text })}
              />
              <TouchableOpacity
                onPress={() => this.onSignInPress()}
                style={styles.button}>
                <Text style={styles.buttonText}>Login</Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => this.onSignUpPress()}
                style={styles.button}>
                <Text style={styles.buttonText}>or Register</Text>
              </TouchableOpacity>
            </View>
          </View>
        </TouchableWithoutFeedback>
      </ScrollView>
    );
  }

  private onSignInPress() {
    if (!this.validateEmail() || !this.validatePassword()) {
      Alert.alert('Incorrect email or password.');
      return;
    }

    let data = new SignInModel();
    data.email = this.state.email;
    data.password = this.state.password;
    this.props.signIn(this.state);
    if (this.props.isLoggedIn) {
      this.props.navigation.navigate('Home');
    }
  }

  private onSignUpPress() {
    this.props.navigation.navigate('SignUp');
  }

  private validateEmail(): boolean {
    let pattern = /^[a-zA-Z0-9]+([-._]?[a-zA-Z0-9]+)*@[a-z0-9]+[-]?([-]?[a-z0-9])+[.]([a-z]{2,})$/;
    return pattern.test(this.state.email);
  }

  private validatePassword(): boolean {
    return (this.state && this.state.password.replace(' ', '') != '')
  }
}

const mapStateToProps = (state: any) => ({
  isLoggedIn: state.auth.isLoggedIn
});

const mapDispatchToProps = (dispatch: any) => ({
  signIn: (data: SignInModel) => dispatch(actions.signIn(data))
});

export default connect(mapStateToProps, mapDispatchToProps)(SignInScreen);
