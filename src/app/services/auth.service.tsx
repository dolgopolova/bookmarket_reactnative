import environment from "../environments/environment";
import SignInModel from "../shared/models/auth/signIn.model";
import SignUpModel from "../shared/models/auth/signUp.model";
import Axios from 'axios';
import TokenAuthModel from "../shared/models/auth/tokenAuth.model";

class AuthService {
    public signIn(userData: SignInModel): Promise<TokenAuthModel> {
        return Axios.post<TokenAuthModel>(`${environment.backendUrl}/auth/signIn`, userData).then((response) => response.data);
    }

    public signUp(userData: SignUpModel): Promise<string> {
        return Axios.post<string>(`${environment.backendUrl}/auth/signUp`, userData).then((response) => response.data);
    }
}
export default new AuthService();