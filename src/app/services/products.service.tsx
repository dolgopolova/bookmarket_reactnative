import ProductModel from "../shared/models/products/product.model";
import Axios from "axios";
import environment from "../environments/environment";

class ProductsService {
    public getAllProducts(): Promise<ProductModel> {
        return Axios.get<ProductModel>(`${environment.backendUrl}/products/getAll`).then((response) => response.data);
    }
}
export default new ProductsService();