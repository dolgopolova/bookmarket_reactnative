interface BaseModel {
    id: string;
    creationDate: string;
}

export default BaseModel;