interface CategoryModel {
    value: string;
    name: string;
}

export default CategoryModel;