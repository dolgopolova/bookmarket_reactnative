import BookModel from "./book.model";
import MagazineModel from "./magazine.model";

interface ProductModel {
    books: BookModel[];
    magazine: MagazineModel[];
}

export default ProductModel;