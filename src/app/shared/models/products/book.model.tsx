import CategoryModel from "./category.model";
import BaseModel from "../base.model";

interface BookModel extends BaseModel {
    name: string;
    authors: string;
    description: string;
    price: number;
    year: number;
    category: CategoryModel;
}

export default BookModel;