import CategoryModel from "./category.model";
import BaseModel from "../base.model";

interface MagazineModel extends BaseModel {
    name: string;
    publisher: string;
    year: number;
    price: number;
    category: CategoryModel;
}

export default MagazineModel;