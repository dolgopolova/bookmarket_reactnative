export default class TokenAuthModel {
    expiresIn: number;
    userId: string;
    accessToken: string;
    firstName: string;
    lastName: string;
    email: string;
}