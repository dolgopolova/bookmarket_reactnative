
export default class ShowError {
    constructor() {
        this.message = '';
        this.shouldShow = false;
    }

    message: string;
    shouldShow: boolean;
}