import { Dimensions } from "react-native";

const React = require("react-native");
const { StyleSheet } = React;
const window = Dimensions.get('window');

export default StyleSheet.create({
    container: {
        width: window.width,
        height: 80,
        borderWidth: 1,
        borderRadius: 1.5,
        borderColor: '#ddd',
        borderBottomWidth: 0,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.8,
        shadowRadius: 2,
        elevation: 1,
        margin: 10
    }
});