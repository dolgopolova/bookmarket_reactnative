import { Component } from "react";
import { connect } from "react-redux";
import React from "react";
import { View, ScrollView } from "react-native";
import CategoryModel from "../../shared/models/products/category.model";
import styles from "../../components/product/style"

interface Props {
    type: "Book" | "Magazine";
    name: string;
    author: string;
    price: number;
    category: CategoryModel;
}

interface State {
}

class ProductComponent extends Component<Props, State> {
    constructor(props: Props) {
        super(props);
    }

    public render() {
        return (
            <ScrollView style={{ flex: 1 }}>
                <View style={styles.container}>

                </View>
            </ScrollView>
        );
    }

}

const mapStateToProps = (state: any) => ({

});

const mapDispatchToProps = (dispatch: any) => ({
});

export default connect(mapStateToProps, mapDispatchToProps)(ProductComponent);