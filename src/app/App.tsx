import React from 'react';
import { createStackNavigator, createAppContainer } from "react-navigation";
import {createStore, applyMiddleware} from "redux";
import { Provider } from "react-redux";
import thunk from "redux-thunk";
import createSagaMiddleware from 'redux-saga'
import SignInScreen from './views/SignIn/signIn.view';
import SignUpScreen from'./views/SignUp/signUp.view';
import MainReducer from './redux/reducers/main.reducer';
import HomeScreen from './views/Home/home.view';
import RootSaga from './redux/sagas/root.saga';

const RootStack = createStackNavigator(
  {
    Home: {
      screen: HomeScreen
    },
    SignIn: {
      screen: SignInScreen
    },
    SignUp: {
      screen: SignUpScreen
    }
  },
  {
    initialRouteName: 'Home'
  }

);

const TopLevelNavigator = createAppContainer(RootStack);
let store = createStore(MainReducer, applyMiddleware(thunk));
// const sagaMiddleware = createSagaMiddleware();
// let store = createStore(MainReducer, applyMiddleware(sagaMiddleware));
// sagaMiddleware.run(RootSaga)

export default class App extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <TopLevelNavigator />
      </Provider>
    );
  }
}